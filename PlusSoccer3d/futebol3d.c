/*
 * Futebol de botao 3D
 *
 * Escrito por:
 * Amanda Oliveira
 * Bruna Pereira
 * Robson Goncalves
 *
 * Projeto para a cadeira de Computacao Grafica
 * Professor Mozart Lemos
 *
 * Universidade La Salle 2018/1

* Navegacao via botoes do mouse + movimento:
* - botao esquerdo: rotaciona objeto
* - botao direito:  aproxima/afasta
* - botao do meio:  translada objeto

* Teclas Home e End fazem zoom in/zoom out
* Teclas 0, 1 e 2 devem ser usadas para escolher a fonte
* de luz desejada (verde, vermelha ou azul)
* Setas movem fonte de luz em x e y
* PageUp/PageDown movem fonte de luz em z

*/
#include "glut.h"
#include <stdlib.h>
#include <curses.h>
#include <stdio.h>
#include <string.h>

// Variaveis para controles de navegacao
GLfloat angle, fAspect;
GLfloat rotX, rotY, rotX_ini, rotY_ini;
GLfloat obsX, obsY, obsZ, obsX_ini, obsY_ini, obsZ_ini;
int x_ini,y_ini,bot,x_ini_placar,y_ini_placar;

// Luz selecionada
int luz = 0;

int xJogadorUm = 0;
int xJogadorUmInicial = 0;
int yJogadorUm = -15;
int yJogadorUmInicial = -15;
int xJogadorDois = 0;
int xJogadorDoisInicial = 0;
int yJogadorDois = 15;
int yJogadorDoisInicial = 15;
int xBola = 0;
int xBolaInicial = 0;
int yBola = 0;
int yBolaInicial = 0;
int xPlacar = -35;
int yPlacar = 35;
int Placar[2] = {0,0};

// Posicao de cada luz
GLfloat posLuz[3][4] = {
	{  0, 50,  0, 1 },
	{  50, 30,  50, 1 },
	{  50, 30,  50, 1 }
};

// Direï¿½ï¿½o de cada luz
GLfloat dirLuz[3][3] = {
	{ 0,-1,0 },
	{ 0,-1,0 },
	{ 0,-1,0 }
};

// Cor difusa de cada luz //RGB
GLfloat luzDifusa[3][4] = {
	{ 0,1,0,1 },
	{ 0,1,0,1 },
	{ 0,1,0,1 }
};

// Cor especular de cada luz //RGB
GLfloat luzEspecular[3][4] = {
	{ 0,0,0,1 },
	{ 0,0,0,1 },
	{ 0,0,0,1 }
};



// funcao que implementa o itoa no gcc
char* itoa(int value, char* result, int base) {
  // verifica se a base e' valida
  if (base < 2 || base > 36) { *result = '\0'; return result; }
  char* ptr = result, *ptr1 = result, tmp_char;
  int tmp_value;

  do {
    tmp_value = value;
    value /= base;
    *ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
  } while ( value );

  // Aplica sinal negativo
  if (tmp_value < 0) *ptr++ = '-';
  *ptr-- = '\0';
  while(ptr1 < ptr) {
    tmp_char = *ptr;
    *ptr--= *ptr1;
    *ptr1++ = tmp_char;
  }
  return result;
}




//___________________________________________________________________
// Funcao responsavel pela especificacao dos parametros de iluminacao
void DefineIluminacao (void)
{
    int i;
	GLfloat luzAmbiente[4]={0.2,0.2,0.2,1.0};
	// Capacidade de brilho do material
	GLfloat especularidade[4]={0.5,0.5,0.5,1.0};
	GLint especMaterial = 90;

	// Define a refletancia do material
	glMaterialfv(GL_FRONT,GL_SPECULAR, especularidade);
	// Define a concentracao do brilho
	glMateriali(GL_FRONT,GL_SHININESS,especMaterial);
	// Ativa o uso da luz ambiente
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, luzAmbiente);

	// Define os parametros das fontes de luz
	for(i=0;i<3;++i)
	{
		glLightfv(GL_LIGHT0+i, GL_AMBIENT, luzAmbiente);
		glLightfv(GL_LIGHT0+i, GL_DIFFUSE, luzDifusa[i] );
		glLightfv(GL_LIGHT0+i, GL_SPECULAR, luzEspecular[i] );
		glLightfv(GL_LIGHT0+i, GL_POSITION, posLuz[i] );
		glLightfv(GL_LIGHT0+i, GL_SPOT_DIRECTION,dirLuz[i]);
		glLightf(GL_LIGHT0 +i, GL_SPOT_CUTOFF,40.0);
		glLightf(GL_LIGHT0 +i, GL_SPOT_EXPONENT,10.0);
	}
}

#define TAM 40
#define D 1
// Funcao para desenhar um "chao" no ambiente
void DesenhaChao(void)
{
	// Flags para determinar a cor de cada quadrado
	//bool flagx,flagz;
	int flagx,flagz;
	float x, z;
	// Define a normal apontando para cima
	glNormal3f(0,1,0);

	glBegin(GL_QUADS);
	  flagx = 0;
	  // X varia de -TAM a TAM, de D em D
	  for(x=-TAM;x<TAM;x+=D)
	  {
		// Flagx determina a cor inicial
		if(flagx) flagz = 0;
		else flagz = 1;
		// Z varia de -TAM a TAM, de D em D
		for(z=-TAM;z<TAM;z+=D)
		{
			// Escolhe cor
			// pinta linhas do campo e gol
			if (x == -TAM+3*D || x == TAM-3*D || x == 0 || z == -TAM+9*D || z == TAM-9*D ||
                x == -TAM+6*D || x == TAM-6*D || z == -TAM+30*D || z == TAM-30*D ||

                // circulo central
                x == TAM-30*D || x == TAM-31*D || x == TAM-32*D || x == TAM-33*D || x == TAM-34*D || x == TAM-35*D ||
                x == TAM-36*D || x == TAM-37*D || x == TAM-38*D || x == TAM-39*D ||

                // circulo central
                x == -TAM+30*D || x == -TAM+31*D || x == -TAM+32*D || x == -TAM+33*D || x == -TAM+34*D || x == -TAM+35*D ||
                x == -TAM+36*D || x == -TAM+37*D || x == -TAM+38*D || x == -TAM+39*D
                )
                    glColor3f(1,1,1);
			else if(flagz) glColor3f(0,0.5,0);
            else glColor3f(0,0.5,0);
            // pinta de verde as linhas brancas do campo e gol que nao queremos que seja branca
            if (x < -TAM+3*D || x > TAM-3*D || z < -TAM+9*D || z > TAM-9*D ||
               (x == -TAM+6*D && z < -TAM+30*D && z != -TAM+9*D) ||
               (x == -TAM+6*D && z > TAM-30*D && z != TAM-9*D) ||
               (x == TAM-6*D && z < -TAM+30*D && z != -TAM+9*D) ||
               (x == TAM-6*D && z > TAM-30*D && z != TAM-9*D) ||
               (x < TAM-6*D && z == -TAM+30*D && x > -TAM+6*D && x != 0) ||
               (x < TAM-6*D && z == TAM-30*D && x > -TAM+6*D && x != 0) ||

               // pinta de verde as linhas do circulo central
               (x == TAM-30*D && (z < TAM-39*D || z > -TAM+39*D) && z != TAM-40*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-31*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+39*D && z != TAM-39*D && z != TAM-40*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-32*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+38*D && z != TAM-38*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-33*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+38*D && z != TAM-38*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-34*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+37*D && z != TAM-37*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-35*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+36*D && z != TAM-36*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-36*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+36*D && z != TAM-36*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-37*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+35*D && z != TAM-35*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-38*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+34*D && z != TAM-34*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == TAM-39*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+34*D && z != TAM-34*D && z != TAM-9*D && z != -TAM+9*D) ||

               // pinta de verde as linhas do circulo central
               (x == -TAM+30*D && (z < TAM-39*D || z > -TAM+39*D) && z != TAM-40*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+31*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+39*D && z != TAM-39*D && z != TAM-40*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+32*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+38*D && z != TAM-38*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+33*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+38*D && z != TAM-38*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+34*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+37*D && z != TAM-37*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+35*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+36*D && z != TAM-36*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+36*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+36*D && z != TAM-36*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+37*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+35*D && z != TAM-35*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+38*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+34*D && z != TAM-34*D && z != TAM-9*D && z != -TAM+9*D) ||
               (x == -TAM+39*D && (z < TAM-38*D || z > -TAM+38*D) && z != -TAM+34*D && z != TAM-34*D && z != TAM-9*D && z != -TAM+9*D)
                )
                    glColor3f(0,0.5,0);
			// E desenha o quadrado
			glVertex3f(x,0,z);
			glVertex3f(x+D,0,z);
			glVertex3f(x+D,0,z+D);
			glVertex3f(x,0,z+D);
			// Alterna cor
			flagz=!flagz;
		}
		// A cada coluna, alterna cor inicial
		flagx=!flagx;
	}
	glEnd();
}



// Funcao callback de redesenho da janela de visualizacao
void Desenha(void)
{
     int i;
	// Limpa a janela de visualizacao com a cor
	// de fundo definida previamente
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Chama a funcao que especifica os parametros de iluminacao
	DefineIluminacao();

	// Desabilita a iluminacao para desenhar as esferas
	glDisable(GL_LIGHTING);
	for(i=0;i<3;++i)
	{
		// Desenha "esferas" nas posiï¿½ï¿½es das fontes de luz
		glPushMatrix();
		glTranslatef(posLuz[i][0],posLuz[i][1],posLuz[i][2]);
		glColor3f(luzDifusa[i][0],luzDifusa[i][1],luzDifusa[i][2]);
		glutSolidSphere(1,5,5);
		glPopMatrix();
	}
	// Habilita iluminaï¿½ï¿½o novamente
	glEnable(GL_LIGHTING);

	// Altera a cor do desenho para branco
	glColor3f(1,1,1);

	// Desenha os elementos e o chao
	glPushMatrix();

	
	// jogador 1
	
	glColor3f(1,0,0);
    glTranslatef(yJogadorUm,1,xJogadorUm);//-7.5,0);
	//glutSolidTorus(5,10,10,10);

	glutSolidSphere(2,100,100);

	glDisable ( GL_TEXTURE_2D );
	glPopMatrix();
	
	

	///Player 1
	/*
	glPushMatrix(); 
	glColor3f(0,1,1);       
	glTranslatef(yJogadorUm +-15,8,xJogadorUm + 0); 
	glScalef(1,2,1); 
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix(); 
	// glColor3f(0,0,0);
	glColor3f(1,0,0);       
	glTranslatef(yJogadorUm +-15,5,xJogadorUm + 0); 
	glScalef(1,4,2); 
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix(); 
	// glColor3f(0,0,0);
	glColor3f(1,0,0);       
	glTranslatef(yJogadorUm +-15,3,xJogadorUm + 1); 
	glScalef(1,4,1); 
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix(); 
	// glColor3f(0,0,0);
	glColor3f(1,0,0);    
	glTranslatef(yJogadorUm +-15,3,xJogadorUm + -1);  
	glScalef(1,4,1); 
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix(); 
	// glColor3f(0,0,0);   
	glColor3f(1,0,0);
	glTranslatef(yJogadorUm +-14,7,xJogadorUm + 1); 
	glScalef(-3,-1,1); 
	glutSolidCube(1);
	glPopMatrix();

	glPushMatrix(); 
	// glColor3f(0,0,0);   
	glColor3f(1,0,0);    
	glTranslatef(yJogadorUm +-14,7,xJogadorUm +-1);  
	glScalef(-3,-1,1);  
	glutSolidCube(1);
	glPopMatrix();
	*/	




	// jogador 2
	glPushMatrix();
	/s elementos 10);
	glColor3f(0,0,1);
    glTranslatef(yJogadorDois,0.5,xJogadorDois);//-7.5,0);
	//glutSolidTorus(5,10,10,10);
	glutSolidSphere(2,100,100);
	glPopMatrix();

    // bola
    glPushMatrix();
	/s elementos 10);
	glColor3f(1,1,1);
    glTranslatef(yBola,0,xBola);//-7.5,0);
	//glutSolidTorus(5,10,10,10);
	glutSolidSphere(1,100,100);
    glPopMatrix();

	DesenhaChao();
    DesenhaPlacar();
	// Executa os comandos OpenGL
	glutSwapBuffers();
	
}

void DesenhaPlacar()
{
    int i=0;

    char placar[70];
    for(i=0; i<70; i++)
        placar[i] = '\0';

    itoa(Placar[0],placar,10);
    itoa(Placar[1],placar,10);
    sprintf(placar, "Inter %d x %d Gremio\0", Placar[0], Placar[1]);
  	glPushMatrix();
        // Posição no universo onde o texto será colocado
        glRasterPos2f(xPlacar, yPlacar);
        // Exibe caracter a caracter
        for(i=0; i<70; i++)
            glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24,placar[i]);
	glPopMatrix();
}

// Funcao usada para especificar a posicao do observador virtual
void PosicionaObservador(void)
{
	// Especifica sistema de coordenadas do modelo
	glMatrixMode(GL_MODELVIEW);
	// Inicializa sistema de coordenadas do modelo
	glLoadIdentity();
	// Posiciona e orienta o observador
	glTranslatef(-obsX,-obsY,-obsZ);
	glRotatef(rotX,1,0,0);
	glRotatef(rotY,0,1,0);
}

// Funcao usada para especificar o volume de visualizacao
void EspecificaParametrosVisualizacao(void)
{
	// Especifica sistema de coordenadas de projecao
	glMatrixMode(GL_PROJECTION);
	// Inicializa sistema de coordenadas de projeaco
	glLoadIdentity();

	// Especifica a projecao perspectiva(angulo,aspecto,zMin,zMax)
	gluPerspective(angle,fAspect,0.5,1500);

	PosicionaObservador();
}

// Funcao callback chamada para gerenciar eventos de teclas normais (ESC)
void Teclado (unsigned char tecla, int x, int y)
{
	if(tecla==27) {// ESC ?
		reproduz_som("acabou.wav");
		exit(0);
	}

	if(tecla>='0' && tecla<='2'){
		luz = tecla - '0';
	} 
	

	// direcoes jogador 1
	else if (tecla==119){ //w // cima jogador 1
        if (xJogadorUm > -34) { // limite do jogador
            xJogadorUm -= 1;
        }

		// tratamento de colisao
        if (xJogadorUm-1 < xBola+1 && xJogadorUm+1 > xBola-1 && (yJogadorUm <= yBola+2 && yJogadorUm >= yBola-2)) {
            if (xBola > -31) { // limite da bola
                xBola -= 1;
            }
        }
	}

	else if (tecla==97){ // a // esquerda jogador 1
        if (yJogadorUm > -40) {
            yJogadorUm -= 1;
        }

		// tratamento de colisao
		if (yJogadorUm-1 < yBola+2 && yJogadorUm+1 > yBola-2 && (xJogadorUm >= xBola && xJogadorUm <= xBola+1)) {
            if (yBola > -37) {
                yBola -= 1;
            }
            if (yBola < -34 && xBola < TAM-30*D && xBola > -TAM+30*D) // trata "colisao" de gol
            {
                gol(2);
            }
		}
	}
	else if (tecla==115){ // s // baixo jogador 1
        if (xJogadorUm < 34) {
            xJogadorUm += 1;
        }

		// tratamento de colisao
        if (xJogadorUm+1 > xBola-1 && xJogadorUm-1 < xBola+1 && (yJogadorUm <= yBola+2 && yJogadorUm >= yBola-2)) {
            if (xBola < 31) {
                xBola += 1;
            }
        }
	}
	else if (tecla==100){ // d // direita jogador 1
        if (yJogadorUm < 40) {
            yJogadorUm += 1;
        }

		// tratamento de colisao
		if (yJogadorUm+1 > yBola-2 && yJogadorUm-2 < yBola+2 && (xJogadorUm >= xBola && xJogadorUm <= xBola+1)) {
            if (yBola < 37) {
                yBola += 1;
            }
            if (yBola > 34 && xBola < TAM-30*D && xBola > -TAM+30*D) // trata "colisao" de gol
            {
                gol(1);
            }
		}
	}


	// jogador 2
	else if (tecla==105){ // i // cima jogador 2
        if (xJogadorDois > -34) {
            xJogadorDois -= 1;
        }

		// tratamento de colisao
		if (xJogadorDois-1 < xBola+1 && xJogadorDois+1 > xBola-1 && (yJogadorDois <= yBola+2 && yJogadorDois >= yBola-2)) {
            if (xBola > -31) {
                xBola -= 1;
            }
        }
	}
	else if (tecla==106){ // j // esquerda jogador 2
        if (yJogadorDois > -40) {
            yJogadorDois -= 1;
        }

		// tratamento de colisao
		if (yJogadorDois-1 < yBola+2 && yJogadorDois+1 > yBola-2 && (xJogadorDois >= xBola && xJogadorDois <= xBola+1)) {
            if (yBola > -37) {
                yBola -= 1;
            }
            if (yBola < -34 && xBola < TAM-30*D && xBola > -TAM+30*D) // trata "colisao" de gol
            {
                gol(2);
            }
		}
	}
	else if (tecla==107){ // k // baixo jogador 2
        if (xJogadorDois < 34) {
            xJogadorDois += 1;
        }

		// tratamento de colisao
		if (xJogadorDois+1 > xBola-1 && xJogadorDois-1 < xBola+1 && (yJogadorDois <= yBola+2 && yJogadorDois >= yBola-2)) {
            if (xBola < 31) {
                xBola += 1;
            }
        }
	}
	else if (tecla==108){ // l // direita jogador 2
        if (yJogadorDois < 40) {
            yJogadorDois += 1;
        }

		// tratamento de colisao
		if (yJogadorDois+1 > yBola-2 && yJogadorDois-1 < yBola+2 && (xJogadorDois >= xBola && xJogadorDois <= xBola+1)) {
            if (yBola < 37) {
                yBola += 1;
            }
            if (yBola > 34 && xBola < TAM-30*D && xBola > -TAM+30*D) // trata "colisao" de gol
            {
                gol(1);
            }
		}
	}

	PosicionaObservador();
	glutPostRedisplay();
	Desenha();
}

void reproduz_som(char *name){


	int playSound( char *filename ) {
		char command[256];
		int status;

		/* create command to execute */
		sprintf( command, "aplay -c 1 -q -t wav %s", filename );

		/* play sound */
		status = system( command );
		
		return status;
	}

	// playSound( "my.wav" );
	playSound( name );




















	// system("my.wav");


	// QSound::play("my.wav");

	/*
   // init FMOD sound system
   FSOUND_Init (44100, 32, 0);

   // load and play mp3
   handle=FSOUND_Sample_Load (0,"my.wav",0, 0, 0);
   FSOUND_PlaySound (0,handle);

   // wait until the users hits a key to end the app
   while (!_kbhit())
   {
   }

   // clean up
   FSOUND_Sample_Free (handle);
   FSOUND_Close();
   */

}


void gol(int time) {
    Placar[time-1] += 1;

	if (Placar[time-1] == 4) {
		reproduz_som("passeio.wav");
	}


	// reproduz_som("my.wav");
	reproduz_som("gol.wav");
	reproduz_som("apito.wav");

    xJogadorUm = xJogadorUmInicial;
    yJogadorUm = yJogadorUmInicial;
    xJogadorDois = xJogadorDoisInicial;
    yJogadorDois = yJogadorDoisInicial;
    xBola = xBolaInicial;
    yBola = yBolaInicial;
// init FMOD sound system
//    FSOUND_Init (44100, 32, 0);
//    // load and play mp3
//    handle=FSOUND_Sample_Load (0,"my.wav",0, 0, 0);
//    FSOUND_PlaySound (0,handle);

//    // wait until the users hits a key to end the app
//    while (!_kbhit())
//    {
//    }

//    // clean up
//    FSOUND_Sample_Free (handle);
//    FSOUND_Close();
}

// Funcao callback para tratar eventos de teclas especiais
// void TeclasEspeciais (int tecla, int x, int y)
void TeclasEspeciais(unsigned char tecla,int x, int y)
{
	// if (tecla == 'a'){
	// 	yJogadorUm -= 1;
	// }
	switch (tecla)
	{
		// case GLUT_KEY_LEFT:
		// 	yJogadorUm -= 1;
		// 	break;
		// case GLUT_KEY_RIGHT:
		// 	yJogadorUm += 1;
		// 	break;
		// case GLUT_KEY_UP:
		// case 'w': //w
		// 	xJogadorUm -= 1;
		// 	if (xJogadorUm-1 < xBola+2 && (yJogadorUm <= yBola+2 && yJogadorUm >= yBola-2)) {
		//  		xBola -= 1;
		//  	}
		// 	break;
		// case GLUT_KEY_DOWN:
		// 	xJogadorUm += 1;
		// 	break;


		// case 'j':
		// 	yJogadorDois -= 1;
		// 	break;
		// case 'l':
		// 	yJogadorDois += 1;
		// 	break;
		// case 'i':
		// 	xJogadorDois -= 1;
        //     if (xJogadorDois-1 < xBola+2 && (yJogadorDois <= yBola+2 && yJogadorDois >= yBola-2)) {
        //         xBola -= 1;
        //     }
		// 	break;
		// case 'k':
		// 	xJogadorDois += 1;
		// 	break;

		case GLUT_KEY_LEFT:			
			posLuz[luz][0] -=2;
			break;
		case GLUT_KEY_RIGHT:		
			posLuz[luz][0] +=2;
			break;
		case GLUT_KEY_UP:			
			posLuz[luz][1] +=2;
			break;
		case GLUT_KEY_DOWN:			
			posLuz[luz][1] -=2;
			break;
		case GLUT_KEY_PAGE_UP:
			posLuz[luz][2] -=2;
			break;
		case GLUT_KEY_PAGE_DOWN:
			posLuz[luz][2] +=2;
			break;
		case GLUT_KEY_HOME:
			if(angle>=10)  angle -=5;
			break;
		case GLUT_KEY_END:
			if(angle<=150) angle +=5;
			break;
	}
	PosicionaObservador();
	glutPostRedisplay();
	Desenha();
}

// Funï¿½o callback para eventos de botoes do mouse
void GerenciaMouse(int button, int state, int x, int y)
{
	if(state==GLUT_DOWN)
	{
		// Salva os parï¿½metros atuais
		x_ini = x;
		y_ini = y;
		x_ini_placar = xPlacar;
		y_ini_placar = yPlacar;
		obsX_ini = obsX;
		obsY_ini = obsY;
		obsZ_ini = obsZ;
		rotX_ini = rotX;
		rotY_ini = rotY;
		bot = button;
	}
	else bot = -1;
}

// Funcao callback para eventos de movimento do mouse
#define SENS_ROT	5.0
#define SENS_OBS	10.0
#define SENS_TRANSL	10.0
void GerenciaMovim(int x, int y)
{
	// Botao esquerdo ?
	if(bot==GLUT_LEFT_BUTTON)
	{
		// Calcula diferenï¿½as
		int deltax = x_ini - x;
		int deltay = y_ini - y;
        // E modifica ï¿½ngulos
		rotY = rotY_ini - deltax/SENS_ROT;

		rotX = rotX_ini - deltay/SENS_ROT;
	}
	// Botao direito ?
	else if(bot==GLUT_RIGHT_BUTTON)
	{
		// Calcula diferenï¿½a
		int deltaz = y_ini - y;
		// E modifica distï¿½ncia do observador
		obsZ = obsZ_ini + deltaz/SENS_OBS;
	}
	// Botï¿½o do meio ?
	else if(bot==GLUT_MIDDLE_BUTTON)
	{
		// Calcula diferencas
		int deltax = x_ini - x;
		int deltay = y_ini - y;
		// E modifica posicoes
		obsX = obsX_ini + deltax/SENS_TRANSL;
		obsY = obsY_ini - deltay/SENS_TRANSL;
	}
	PosicionaObservador();
	glutPostRedisplay();
}

// Funcao callback chamada quando o tamanho da janela e' alterado
void AlteraTamanhoJanela(GLsizei w, GLsizei h)
{
	// Para previnir uma divisao por zero
	if ( h == 0 ) h = 1;

	// Especifica as dimensï¿½es da viewport
	glViewport(0, 0, w, h);

	// Calcula a correï¿½ï¿½o de aspecto
	fAspect = (GLfloat)w/(GLfloat)h;

	EspecificaParametrosVisualizacao();
}

// funcao responsavel por inicializar parï¿½metros e variï¿½veis
void Inicializa (void)
{
	// Define a cor de fundo da janela de vizualizacao como branca
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	// Habilita a definiï¿½ï¿½o da cor do material a partir da cor corrente
	glEnable(GL_COLOR_MATERIAL);
	//Habilita o uso de iluminaï¿½ï¿½o
	glEnable(GL_LIGHTING);
	// Habilita as fontes de luz
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_LIGHT2);
	// Habilita o depth-buffering
	glEnable(GL_DEPTH_TEST);

	// Habilita o modelo de colorizaï¿½ï¿½o de Gouraud
	glShadeModel(GL_SMOOTH);

	// Inicializa a variï¿½vel que especifica o ï¿½ngulo da projeï¿½ï¿½o
	// perspectiva
	angle=50;

	// Inicializa as variï¿½veis usadas para alterar a posicao do
	// observador virtual
	rotX = 40;
	rotY = 0;
	obsX = obsY = 0;
	obsZ = 100;
}

// Programa Principal
int main (int argc, char** argv)
{
	reproduz_som("apito.wav");
	
	glutInit (&argc, argv);
	// Define o modo de operacao da GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);

	// Especifica a posicao inicial da janela GLUT
	glutInitWindowPosition(5,5);

	// Especifica o tamanho inicial em pixels da janela GLUT
	glutInitWindowSize(500,500);

	// Cria a janela passando como argumento o titulo da mesma
	glutCreateWindow("Super Futebol de Botao 3D :D");

	// Registra a funcao callback de redesenho da janela de vizualizacao
	glutDisplayFunc(Desenha);

	// Registra a funcao callback de redimensionamento da janela de vizualizacao
	glutReshapeFunc(AlteraTamanhoJanela);

	// Registra a funcao callback para tratamento das teclas normais
	glutKeyboardFunc (Teclado);

	// Registra a funcao callback para tratamento das teclas especiais
	glutSpecialFunc (TeclasEspeciais);

	// Registra a funcao callback para eventos de botoes do mouse
	glutMouseFunc(GerenciaMouse);

	// Registra a funcao callback para eventos de movimento do mouse
	glutMotionFunc(GerenciaMovim);

	// Chama a funcao responsavel por fazer as inicializacoes
	Inicializa();

	// Inicia o processamento e aguarda interacao do usuario
	glutMainLoop();

	return 0;
}
