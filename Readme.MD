# Trabalho de Computação Gráfica Universidade La Salle 2018/1 G2 ##

### Integrantes: ###
### Amanda Oliveira ###
### Bruna Pereira ###
### Robson Gonçalves ###



![picture](img/game0.png)
![picture2](img/game1.png)
![picture3](img/game2.png)


# Definição do trabalho prático para G2 - apresentação em 10/07

2D to 3D - Game 

O objetivo do trabalho de G1 era desenvolver um ambiente bidimensional onde o usuário possa interagir,  
“navegando” transformando elementos dentro do ambiente através de translação, rotação e escala. A  
aplicação deverá permitir que o usuário interaja através do teclado ou mouse.  
A idéia do ambiente 2D deve ser inspirada no jogo de futebol de botão. O aluno deve modelar o  
campo, (no mínimo) dois botões e a bola, com a movimentação de botões e bola. Os movimentos  
devem ser sincronizados de maneira a imitar uma movimentação (“chute”) do jogo real controlados  
pelos dispositivos de entrada.

# Definição do trabalho de G2 

O objetivo deste trabalho é desenvolver uma aplicação que permita a modelagem de um ambiente tridimensional onde o usuário possa interagir, navegando no ambiente.   
A aplicação deverá permitir que o usuário interaja através de teclado, mouse ou qualquer outro dispositivo de entrada.  
Essa interação deve possibilitar a manipulação da câmera sintética, fazendo com que o usuário percorra o ambiente.  
Além de roll, elevation e azimute, a câmera deve percorrer o ambiente em todas as direções.  

A idéia é modelar uma extensão do trabalho 2D desenvolvido no G1, porém, o ambiente deve ser 3D!  

# Elementos Obrigatórios 
Alguns elementos obrigatórios no projeto:  
· 2 ou mais objetos 3D criados pelo aluno (definindo os vertices);  
· 2 ou mais objetos modelados pelo OpenGL (glWireCube(), glSolidSphere(), ...);  
- Possibilidade de visualisar com projeção em perspectiva ou paralela;  

· Manipulação de Câmera virtual roll, elevation e azimute (já está implementado no exemplo de inserção de objetos .obj  
 – ver no Moodle em exemplos aula 3 G2 – abrir no devcpp ou CodeBlocks o arquivo importObj.dev   
 – LEIA COM ATENÇÃO O CABEÇALHO DO ARQUIVO Import3D.c;  
· Implementação de uma ou mais técnicas de renderização (além da cor);  
· Movimento das peças do game!  

# Programa
Implementar usando a biblioteca OpenGL, e a linguagem de programação C.  

O programa fonte deverá ser entregue, com comentários de lógica de programação e com o nome dos alunos no cabeçalho.   
Esse fonte também deverá ser entregue via Moodle onde o padrão do nome T2-CG– nomesdosalunos .   
O nome do arquivo também deve ser padronizado como: NomesAlunos-cg-t2.c  

# Avaliação
Durante a apresentação, para o professor e para os colegas (10 minutos), o grupo deve relatar brevemente a abordagem utilizada para a implementação do ambiente,   comentando técnicas utilizadas, relatando problemas e soluções encontradas e mostrar o projeto 2D do G1 para comparar.   
Todos os integrantes do grupo deverão apresentar o trabalho!  

Critérios para Avaliação:  

1. Elementos obrigatórios 30%  
2. Código fonte organizado, comentado e identado 10%  
3. Apresentação do trabalho 20%  
4. Funcionamento 20%  
5. Criatividade 20%  

# Observações:
· O trabalho deverá manter o grupo do G1.  
· Trabalhos iguais ou muito parecidos serão considerados cópias, sendo as notas divididas ou zeradas.  
O programa deverá ser apresentado no dia : 10/07  

####################################### TABELA ASCII #######################################
[https://theasciicode.com.ar/ascii-printable-characters/lowercase-letter-w-minuscule-ascii-code-119.html](https://theasciicode.com.ar/ascii-printable-characters/lowercase-letter-w-minuscule-ascii-code-119.html)
_________________________________________________________________________________________________________________________
####################################### I N S T RU Ç O E S L I N U X #######################################
Link das instruções para compilar Open GPL no Linux:  

[https://pt.wikibooks.org/wiki/Programa%C3%A7%C3%A3o_com_OpenGL/Instala%C3%A7%C3%A3o/Linux](https://pt.wikibooks.org/wiki/Programa%C3%A7%C3%A3o_com_OpenGL/Instala%C3%A7%C3%A3o/Linux  )


Copiar o arquivo glut.h par a pasta do projeto    
Usar as bibliotecas  
#include "glut.h"


#include 

Compilar com o comando:  
g++ inicio.cpp -o inicio -lGL -lGLU -lglut  

ou  

gcc inicio.c -o inicio -lGL -lGLU -lglut  
gcc futebol3d.c -o futebol3d -lGL -lGLU -lglut





g++ futebol.c -o futebol -lGL -lGLU -lglut  

gcc futebol.c -o futebol -lGL -lGLU -lglut  